﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinObject : MonoBehaviour {
	public AudioSource sonido;

	void Start(){
		//sonido = AudioSource; //GetComponent<AudioSource> ();
	}

	void OnTriggerEnter2D(){
		Debug.Log ("Choque");
		sonido.Play ();
		FindObjectOfType<GameManager>().Win ();
		Destroy (gameObject);
		gameObject.SetActive (false);
	}
}
