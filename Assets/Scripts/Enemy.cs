﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public float velX=5;

	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity = new Vector2 (velX, rb.velocity.y);
		Debug.Log (velX);
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.collider.tag == "cambioDireccion") {
			Debug.Log ("change");
			velX = -velX;
			var escala = transform.localScale;
			escala.x *= -1;
			transform.localScale = escala;
		}
	}
}
