﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public GameObject winText;
	public GameObject loseText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Win(){
		winText.SetActive (true);
		Invoke ("CargarNivel",2f);
	}

	public void Lose(){
		loseText.SetActive (true);
		Invoke ("Reset",2f);
	}

	void Reset(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	void CargarNivel(){
		string nivelSgte;
		switch (SceneManager.GetActiveScene ().name) {
		case "Escena 1":
			nivelSgte = "Escena 2";
			break;
		default:
			nivelSgte = "Escena 2";
			break;
		}
		SceneManager.LoadScene(nivelSgte);
	}
}
