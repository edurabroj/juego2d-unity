﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class PlayerHandler : MonoBehaviour {
	Rigidbody2D rb;
	Animator animator;
	public float velX=8;
	public float salto=320;
	public int nroSaltos=2;
	private int nroSaltosDados = 0;
	// Use this for initialization
	private float velocity;

	//variables caida
	private float alturaWhenJump;
	private float alturaWhenFall;
	public float alturaMuerte;

	//audio
	AudioSource sonido;


	//no perder puntaje
	/*public static PlayerHandler ph;
	void Awake(){
		if (ph == null) {
			ph = this;
			DontDestroyOnLoad (gameObject);
		} else if (ph != this) {
			Destroy (gameObject);
		}
	}*/

	//puntaje
	public int puntajeMatarEnemigo;
	int puntaje=0;
	public Text textoPuntaje;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		sonido = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if(!animator.GetBool("gameOver")){
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				if (transform.localScale.x < 0)
					mirarOtroLado ();
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				velocity = velX;
				if (Input.GetKey (KeyCode.LeftShift)) {
					velocity += velX;
				}
			}
			
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				velocity = 0;
			}

			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				if (transform.localScale.x > 0)
					mirarOtroLado ();
			}
				
			if (Input.GetKey (KeyCode.LeftArrow)) {
				velocity = -velX;
				if (Input.GetKey (KeyCode.LeftShift)) {
					velocity -= velX;
				}
			}
			
			if (Input.GetKeyUp (KeyCode.LeftArrow))
				velocity = 0;

			//saltar
			if (nroSaltosDados < nroSaltos)
			if (Input.GetKeyDown (KeyCode.Space)) {
				sonido.Play ();
				rb.velocity = new Vector2 (0, salto);
				animator.SetBool ("enAire", true);
				nroSaltosDados++;
			}

			//slide
			if (Input.GetKeyDown (KeyCode.LeftControl)) {
				animator.SetBool ("sliding", true);
			}
			if (Input.GetKeyUp (KeyCode.LeftControl)) {
				animator.SetBool ("sliding", false);
			}
		}

		settearVelocidad ();
		MoverPersonaje (velocity);
		wrapperar ();
		perdio ();
	}
	void perdio(){
		if(animator.GetFloat ("vida")<0.1){
			animator.SetBool("gameOver",true);
			FindObjectOfType<GameManager>().Lose ();
			var tempVelocity = rb.velocity;
			tempVelocity.x = 0;
			rb.velocity = tempVelocity;
		}
	}

	private void MoverPersonaje(float velocidad){
		rb.velocity = new Vector2 (velocidad, rb.velocity.y);
	}
	void OnCollisionEnter2D(Collision2D collision){
		if (collision.collider.tag == "piso") {
			animator.SetBool ("enAire",false);
			nroSaltosDados = 0;
			alturaWhenFall = alturaWhenJump - transform.position.y;
			Debug.Log (alturaWhenFall);
			if (alturaWhenFall >= alturaMuerte) {
				animator.SetFloat ("vida", animator.GetFloat ("vida") - 200);
			}
		}
		if (collision.collider.tag == "enemy") {
			animator.SetFloat ("vida", animator.GetFloat ("vida") - 200);
			animator.SetBool ("gameOver", true);
		}
		//matar enemigo
		if (collision.collider.tag == "enemyCabeza") {
			Destroy (collision.collider.transform.parent.gameObject);
			puntaje += puntajeMatarEnemigo;
			textoPuntaje.text=puntaje.ToString();
			/*velX = -velX;
			var escala = transform.localScale;
			escala.x *= -1;
			transform.localScale = escala;*/
		}
	}
	void OnCollisionExit2D(Collision2D collision){
		if (collision.collider.tag == "enemy") {
			animator.SetFloat ("vida", animator.GetFloat ("vida") + 200);
		}
		if (collision.collider.tag == "piso") {
			animator.SetBool ("enAire",true);
			alturaWhenJump = transform.position.y;
		}
	}

	void mirarOtroLado ()
	{
		//transform.localScale = new Vector3 (-1*transform.localScale.x, transform.localScale.y, transform.localScale.z);
		//if ((transform.localScale.x > 0 && velocity < 0) || (transform.localScale.x < 0 && velocity > 0)) {
			var escala = transform.localScale;
			escala.x *= -1;
			transform.localScale = escala;
		//}
	}

	void settearVelocidad(){
		animator.SetFloat ("velocidad",Math.Abs(rb.velocity.x));
		animator.SetFloat ("velY",rb.velocity.y);
	}

	void wrapperar()
	{
		var pos = Math.Abs (transform.position.x);
		//Debug.Log (pos);
		if ( pos > 9.8) {
			var transTemp = transform.position;
			transTemp.x *= -1;
			if(transTemp.x <0)
				transTemp.x += 0.1f;
			else
				transTemp.x -= 0.1f;				
			transform.position = transTemp;
		}
	}
}
